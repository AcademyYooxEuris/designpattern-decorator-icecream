﻿using System;

namespace DecoratorIcecream
{
    class Program
    {
        static void Main(string[] args)
        {
            var simpleIceCream = new SimpleIceCream();
            var honeyIceCream = new HoneyIceCream(simpleIceCream);
            Console.WriteLine(honeyIceCream.MakeIceCream());

            var nutsAndHoneyIceCream = new NutsIceCream(honeyIceCream);
            Console.WriteLine(nutsAndHoneyIceCream.MakeIceCream());

            Console.ReadLine();
        }
    }
}
